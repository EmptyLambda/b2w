const N: int;

axiom 0 <= N;

procedure not_verify()
  ensures (forall k, l: int :: 0 <= k && k <= l && l < N ==> N < N);
{
	var x: int;
	x := -N;
	while (x != x) {
	}
}
