package boogie2WhyML.Helper.CallForall;


import boogie.ProgramFactory;
import boogie.ast.*;
import boogie.ast.asttypes.ArrayAstType;
import boogie.ast.asttypes.NamedAstType;
import boogie.ast.asttypes.PrimitiveAstType;
import boogie.ast.astvisitor.ASTVisitor;
import boogie.ast.declaration.*;
import boogie.ast.expression.*;
import boogie.ast.expression.literal.*;
import boogie.ast.specification.EnsuresSpecification;
import boogie.ast.specification.LoopInvariantSpecification;
import boogie.ast.specification.ModifiesSpecification;
import boogie.ast.specification.RequiresSpecification;
import boogie.ast.statement.*;

import java.util.LinkedHashMap;
import java.util.Map;

public class CallForallInfoVisitor implements ASTVisitor {

    /**
     * creats a map from procedure name to CallForallInfo, which can be used to translate call-forall statements into assume statements.
     * @param declarations
     */
    public CallForallInfoVisitor(Declaration[] declarations){
        callForallInfoMap = new LinkedHashMap<>();
        //only the "procedure" visitor does something.
        for(Declaration d: declarations){
            d.accept(this);
        }
    }

    private Map<String,CallForallInfo> callForallInfoMap;



    @Override
    public void visit(BitvecLiteral bitvecLiteral) {
        //nothing
    }

    @Override
    public void visit(BooleanLiteral booleanLiteral) {
        //nothing
    }

    @Override
    public void visit(IntegerLiteral integerLiteral) {
        //nothing
    }

    @Override
    public void visit(RealLiteral realLiteral) {

    }

    @Override
    public void visit(StringLiteral stringLiteral) {

    }

    @Override
    public void visit(ArrayAccessExpression arrayAccessExpression) {

    }

    @Override
    public void visit(ArrayStoreExpression arrayStoreExpression) {

    }

    @Override
    public void visit(BinaryExpression binaryExpression) {

    }

    @Override
    public void visit(BitVectorAccessExpression bitVectorAccessExpression) {

    }

    @Override
    public void visit(CodeExpression codeExpression) {

    }

    @Override
    public void visit(FunctionApplication functionApplication) {

    }

    @Override
    public void visit(FunctionApplicationWithType functionApplicationWithType) {

    }

    @Override
    public void visit(IdentifierExpression identifierExpression) {

    }

    @Override
    public void visit(IfThenElseExpression ifThenElseExpression) {

    }

    @Override
    public void visit(QuantifierExpression quantifierExpression) {

    }

    @Override
    public void visit(LambdaExpression lambdaExpression) {

    }

    @Override
    public void visit(UnaryExpression unaryExpression) {

    }

    @Override
    public void visit(WildcardExpression wildcardExpression) {

    }

    @Override
    public void visit(EnsuresSpecification ensuresSpecification) {

    }

    @Override
    public void visit(LoopInvariantSpecification loopInvariantSpecification) {

    }

    @Override
    public void visit(ModifiesSpecification modifiesSpecification) {

    }

    @Override
    public void visit(RequiresSpecification requiresSpecification) {

    }

    @Override
    public void visit(AssertStatement assertStatement) {

    }

    @Override
    public void visit(AssignmentStatement assignmentStatement) {

    }

    @Override
    public void visit(AssumeStatement assumeStatement) {

    }

    @Override
    public void visit(BreakStatement breakStatement) {

    }

    @Override
    public void visit(CallStatement callStatement) {

    }

    @Override
    public void visit(GotoStatement gotoStatement) {

    }

    @Override
    public void visit(HavocStatement havocStatement) {

    }

    @Override
    public void visit(IfStatement ifStatement) {

    }

    @Override
    public void visit(Label label) {

    }

    @Override
    public void visit(ParallelCall parallelCall) {

    }

    @Override
    public void visit(ReturnStatement returnStatement) {

    }

    @Override
    public void visit(WhileStatement whileStatement) {

    }

    @Override
    public void visit(YieldStatement yieldStatement) {

    }

    @Override
    public void visit(ArrayLHS arrayLHS) {

    }

    @Override
    public void visit(Body body) {

    }

    @Override
    public void visit(NamedAttribute namedAttribute) {

    }

    @Override
    public void visit(ParentEdge parentEdge) {

    }

    @Override
    public void visit(Project project) {

    }

    @Override
    public void visit(Trigger trigger) {

    }

    @Override
    public void visit(Unit unit) {

    }

    @Override
    public void visit(VariableLHS variableLHS) {

    }

    @Override
    public void visit(VarList varList) {

    }

    @Override
    public void visit(Axiom axiom) {

    }

    @Override
    public void visit(ConstDeclaration constDeclaration) {

    }

    @Override
    public void visit(FunctionDeclaration functionDeclaration) {

    }

    @Override
    public void visit(Implementation implementation) {

    }

    /**
     * returns true, if @param procedureDeclaration is a valid procedure which can be used as a call forall.
     * (i.e. no output arguments, empty body.)
     * @param procedureDeclaration
     * @return
     */
    private boolean isValidProcedureForCallForall(ProcedureDeclaration procedureDeclaration){
        return (procedureDeclaration.getOutParams().length == 0
                && procedureDeclaration.getBody() != null
                && procedureDeclaration.getBody().getBlock() != null
                && procedureDeclaration.getBody().getBlock().length == 0);
    }

    @Override
    public void visit(ProcedureDeclaration procedureDeclaration) {
        if(isValidProcedureForCallForall(procedureDeclaration)){
            CallForallInfo callForallInfo = new CallForallInfo(procedureDeclaration.getInParams(),procedureDeclaration.getSpecification());
            callForallInfoMap.put(procedureDeclaration.getIdentifier(),callForallInfo);
        }
    }

    @Override
    public void visit(TypeDeclaration typeDeclaration) {

    }

    @Override
    public void visit(VariableDeclaration variableDeclaration) {

    }

    @Override
    public void visit(ArrayAstType arrayAstType) {

    }

    @Override
    public void visit(NamedAstType namedAstType) {

    }

    @Override
    public void visit(PrimitiveAstType primitiveAstType) {

    }
}
