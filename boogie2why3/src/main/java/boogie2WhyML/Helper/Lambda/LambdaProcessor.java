package boogie2WhyML.Helper.Lambda;

import boogie.ast.Attribute;
import boogie.ast.VarList;
import boogie.ast.asttypes.ASTType;
import boogie.ast.asttypes.ArrayAstType;
import boogie.ast.asttypes.PrimitiveAstType;
import boogie.ast.declaration.Axiom;
import boogie.ast.declaration.ConstDeclaration;
import boogie.ast.declaration.Declaration;
import boogie.ast.declaration.FunctionDeclaration;
import boogie.ast.expression.*;
import boogie.enums.BinaryOperator;
import boogie.type.BoogieType;

import java.util.LinkedList;
import java.util.List;

/**
 * process a lambda expression and generate a new function declaration, and an axiom.
 */
public class LambdaProcessor {

    /**
     * given a lambda expression, it returns a Constant declaration and an axiom, so that tis constant can be used instead of the lambda
     * first element is always the constant declaration
     * expression.
     * @param lambdaExpression
     * @param functionName
     * @return
     */
    protected static List<Declaration> generateLambdaDeclarations(LambdaExpression lambdaExpression, String functionName){
        List<Declaration> declarations = new LinkedList<>();
        //1. add function with correct type depending on lambda expression.
        List<ASTType> indexTypesList = new LinkedList<>();
        for(VarList varList: lambdaExpression.getParameters()){
            int identCount = varList.getIdentifiers().length;
            if(identCount == 0){
                //parameters shouldn't be able to be unname, but just in case
                indexTypesList.add(varList.getType());
            }
            for(int i = 0; i < identCount; ++i){
                indexTypesList.add(varList.getType());
            }
        }
        ASTType valueType = BoogieTypeConverter.convertBoogieType(lambdaExpression.getSubExpression().getType().getUnderlyingType());
        ASTType outType =  new ArrayAstType(null,lambdaExpression.getType(),lambdaExpression.getTypeParams(), indexTypesList.toArray(new ASTType[indexTypesList.size()]),valueType);

        VarList outParameter = new VarList(null,new String[0],outType);
        FunctionDeclaration functionDeclaration = new FunctionDeclaration(null,lambdaExpression.getAttributes(),functionName,lambdaExpression.getTypeParams(),new VarList[0],outParameter);

        declarations.add(functionDeclaration);

        //2. add axiom for generated function.
        VarList[] quantifierParameters = lambdaExpression.getParameters();
        String[] quantifierTypeParams = lambdaExpression.getTypeParams();
        List<Expression> arrayAccessIndices = new LinkedList<>();
        for(VarList varList : quantifierParameters){
            for(String ident : varList.getIdentifiers()){
                arrayAccessIndices.add(new IdentifierExpression(null,varList.getType().getBoogieType().getUnderlyingType(),ident));
            }
        }
        Expression arrayExpression = new FunctionApplication(null, lambdaExpression.getType().getUnderlyingType(),functionDeclaration.getIdentifier(),new Expression[0]);

        Expression leftExpression = new ArrayAccessExpression(null, lambdaExpression.getSubExpression().getType(),arrayExpression,arrayAccessIndices.toArray(new Expression[arrayAccessIndices.size()]));
        Expression rightExpression = lambdaExpression.getSubExpression();
        Expression equalityExpression = new BinaryExpression(null,rightExpression.getType(), BinaryOperator.COMPEQ,leftExpression,rightExpression);
        Expression quantifiedExpression = new QuantifierExpression(null, BoogieType.boolType,true,quantifierTypeParams,quantifierParameters,new Attribute[0],equalityExpression);
        Axiom lambdaAxiom = new Axiom(null,new Attribute[0],quantifiedExpression);
        declarations.add(lambdaAxiom);

        return declarations;
    }

}
