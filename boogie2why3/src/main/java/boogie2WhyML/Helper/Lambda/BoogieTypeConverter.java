package boogie2WhyML.Helper.Lambda;


import boogie.ast.asttypes.ASTType;
import boogie.ast.asttypes.PrimitiveAstType;
import boogie.type.BoogieType;
import boogie.type.PrimitiveType;

public class BoogieTypeConverter {


    /**
     * given a boogieType, returns an equivalent AST Type.
     * @param boogieType
     * @return
     */
    public static ASTType convertBoogieType(BoogieType boogieType){

        if(boogieType instanceof PrimitiveType){
            PrimitiveType primitiveType = (PrimitiveType)boogieType;
            return new PrimitiveAstType(null,primitiveType.toString());
        }else{
            throw new RuntimeException("implement conversion fro boogieType to ASTType.");
        }

    }

}
