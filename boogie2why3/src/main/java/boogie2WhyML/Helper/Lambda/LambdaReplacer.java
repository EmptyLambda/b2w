package boogie2WhyML.Helper.Lambda;


import boogie.ProgramFactory;
import boogie.ast.*;
import boogie.ast.asttypes.ArrayAstType;
import boogie.ast.asttypes.NamedAstType;
import boogie.ast.asttypes.PrimitiveAstType;
import boogie.ast.astvisitor.ASTVisitor;
import boogie.ast.declaration.*;
import boogie.ast.expression.*;
import boogie.ast.expression.literal.*;
import boogie.ast.specification.EnsuresSpecification;
import boogie.ast.specification.LoopInvariantSpecification;
import boogie.ast.specification.ModifiesSpecification;
import boogie.ast.specification.RequiresSpecification;
import boogie.ast.statement.*;
import util.Log;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * replace lambda expressions by equivalent axiomatized functions.
 */
public class LambdaReplacer implements ASTVisitor{

    /**
     * automatically replace all lambda expressions in the delcarations
     * TODO, for now, it only works within function bodies (1. level, i.e. not if it is within if then else), and not yet within procedure bodies.
     * @param programFactory
     */
    public LambdaReplacer(ProgramFactory programFactory){
        this.newDeclarations = new LinkedList<>();
        this.newDeclarations.addAll(Arrays.asList(programFactory.getASTRootWithoutModfiesClause().getDeclarations()));
        for(Declaration d: programFactory.getASTRootWithoutModfiesClause().getDeclarations()){
            d.accept(this);
        }
        programFactory.getASTRootWithoutModfiesClause().setDeclarations(newDeclarations.toArray(new Declaration[newDeclarations.size()]));
    }

    private List<Declaration> newDeclarations;


    @Override
    public void visit(BitvecLiteral bitvecLiteral) {

    }

    @Override
    public void visit(BooleanLiteral booleanLiteral) {

    }

    @Override
    public void visit(IntegerLiteral integerLiteral) {

    }

    @Override
    public void visit(RealLiteral realLiteral) {

    }

    @Override
    public void visit(StringLiteral stringLiteral) {

    }

    @Override
    public void visit(ArrayAccessExpression arrayAccessExpression) {

    }

    @Override
    public void visit(ArrayStoreExpression arrayStoreExpression) {

    }

    @Override
    public void visit(BinaryExpression binaryExpression) {

    }

    @Override
    public void visit(BitVectorAccessExpression bitVectorAccessExpression) {

    }

    @Override
    public void visit(CodeExpression codeExpression) {

    }

    @Override
    public void visit(FunctionApplication functionApplication) {

    }

    @Override
    public void visit(FunctionApplicationWithType functionApplicationWithType) {

    }

    @Override
    public void visit(IdentifierExpression identifierExpression) {

    }

    @Override
    public void visit(IfThenElseExpression ifThenElseExpression) {

    }

    @Override
    public void visit(QuantifierExpression quantifierExpression) {

    }

    @Override
    public void visit(LambdaExpression lambdaExpression) {

    }

    @Override
    public void visit(UnaryExpression unaryExpression) {

    }

    @Override
    public void visit(WildcardExpression wildcardExpression) {

    }

    @Override
    public void visit(EnsuresSpecification ensuresSpecification) {

    }

    @Override
    public void visit(LoopInvariantSpecification loopInvariantSpecification) {

    }

    @Override
    public void visit(ModifiesSpecification modifiesSpecification) {

    }

    @Override
    public void visit(RequiresSpecification requiresSpecification) {

    }

    @Override
    public void visit(AssertStatement assertStatement) {

    }

    @Override
    public void visit(AssignmentStatement assignmentStatement) {
        for(LeftHandSide lhs: assignmentStatement.getLhs()){
            lhs.accept(this);
        }
        for(int i = 0; i < assignmentStatement.getRhs().length; ++i){
            Expression currentExpression = assignmentStatement.getRhs()[i];
            if(currentExpression instanceof LambdaExpression){
                assignmentStatement.getRhs()[i] = lambdaReplacement((LambdaExpression)currentExpression,newDeclarations);
            }else{
                currentExpression.accept(this);
            }
        }
    }

    @Override
    public void visit(AssumeStatement assumeStatement) {

    }

    @Override
    public void visit(BreakStatement breakStatement) {

    }

    @Override
    public void visit(CallStatement callStatement) {

    }

    @Override
    public void visit(GotoStatement gotoStatement) {

    }

    @Override
    public void visit(HavocStatement havocStatement) {

    }

    @Override
    public void visit(IfStatement ifStatement) {
        if(ifStatement.getCondition() instanceof LambdaExpression){
            Log.error("lambda expression not replaced in if statement condition.");
            //ifStatement.setCondition(lambdaReplacement((LambdaExpression)ifStatement.getCondition(),newDeclarations));
        }
        for(Statement s: ifStatement.getThenPart()){
            s.accept(this);
        }
        for(Statement s: ifStatement.getElsePart()){
            s.accept(this);
        }
    }

    @Override
    public void visit(Label label) {

    }

    @Override
    public void visit(ParallelCall parallelCall) {

    }

    @Override
    public void visit(ReturnStatement returnStatement) {

    }

    @Override
    public void visit(WhileStatement whileStatement) {

    }

    @Override
    public void visit(YieldStatement yieldStatement) {

    }

    @Override
    public void visit(ArrayLHS arrayLHS) {

    }

    @Override
    public void visit(Body body) {
        for(Statement s: body.getBlock()){
            s.accept(this);
        }
    }

    @Override
    public void visit(NamedAttribute namedAttribute) {

    }

    @Override
    public void visit(ParentEdge parentEdge) {

    }

    @Override
    public void visit(Project project) {

    }

    @Override
    public void visit(Trigger trigger) {

    }

    @Override
    public void visit(Unit unit) {

    }

    @Override
    public void visit(VariableLHS variableLHS) {

    }

    @Override
    public void visit(VarList varList) {

    }

    @Override
    public void visit(Axiom axiom) {

    }

    @Override
    public void visit(ConstDeclaration constDeclaration) {

    }
    private int lambdaCounter = 0;

    private String nextFunctionName(){
        return "lambda"+(lambdaCounter++);
    }


    /**
     * returns a function application which can be used to replace the given lambda expression.
     * Adds the necessary function declaration and application to the newDeclarations.
     * @param lambdaExpression
     * @param newDeclarations
     * @return
     */
    private FunctionApplication lambdaReplacement(LambdaExpression lambdaExpression, List<Declaration> newDeclarations){
        List<Declaration> newFunctionDeclarations = LambdaProcessor.generateLambdaDeclarations(lambdaExpression,nextFunctionName());
        newDeclarations.addAll(newFunctionDeclarations);
        FunctionDeclaration newFunctionDeclaration = (FunctionDeclaration)newFunctionDeclarations.get(0);
        return new FunctionApplication(null,newFunctionDeclaration.getIdentifier(),new Expression[0]);
    }

    @Override
    public void visit(FunctionDeclaration functionDeclaration) {
        if(functionDeclaration.getBody() instanceof LambdaExpression){
            /*//TODO REMOVE
            LambdaExpression lambdaExpression = (LambdaExpression)functionDeclaration.getBody();
            List<Declaration> newFunctionDeclarations = LambdaProcessor.generateLambdaDeclarations(lambdaExpression,nextFunctionName());
            newDeclarations.addAll(newFunctionDeclarations);
            FunctionDeclaration newFunctionDeclaration = (FunctionDeclaration)newFunctionDeclarations.get(0);
            functionDeclaration.setBody(new FunctionApplication(null,newFunctionDeclaration.getIdentifier(),new Expression[0]));
            */
            LambdaExpression lambdaExpression = (LambdaExpression)functionDeclaration.getBody();
            functionDeclaration.setBody(lambdaReplacement(lambdaExpression,newDeclarations));
        }else{
            if(functionDeclaration.getBody() != null){
                functionDeclaration.getBody().accept(this);
            }
        }
    }

    @Override
    public void visit(Implementation implementation) {
        if(implementation.getBody() != null){
            //this shouldnt be null, but doesn't hurt to check.
            implementation.getBody().accept(this);
        }
    }

    @Override
    public void visit(ProcedureDeclaration procedureDeclaration) {
        if(procedureDeclaration.getBody() != null){
            procedureDeclaration.getBody().accept(this);
        }
    }

    @Override
    public void visit(TypeDeclaration typeDeclaration) {

    }

    @Override
    public void visit(VariableDeclaration variableDeclaration) {

    }

    @Override
    public void visit(ArrayAstType arrayAstType) {

    }

    @Override
    public void visit(NamedAstType namedAstType) {

    }

    @Override
    public void visit(PrimitiveAstType primitiveAstType) {

    }
}
