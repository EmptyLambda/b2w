package boogie2WhyML.Helper;

import boogie.ProgramFactory;
import boogie.ast.*;
import boogie.ast.asttypes.ASTType;
import boogie.ast.asttypes.ArrayAstType;
import boogie.ast.asttypes.NamedAstType;
import boogie.ast.asttypes.PrimitiveAstType;
import boogie.ast.astvisitor.ASTVisitor;
import boogie.ast.declaration.*;
import boogie.ast.declaration.Axiom;
import boogie.ast.declaration.Declaration;
import boogie.ast.declaration.FunctionDeclaration;
import boogie.ast.declaration.TypeDeclaration;
import boogie.ast.expression.*;
import boogie.ast.expression.literal.*;
import boogie.ast.specification.EnsuresSpecification;
import boogie.ast.specification.LoopInvariantSpecification;
import boogie.ast.specification.ModifiesSpecification;
import boogie.ast.specification.RequiresSpecification;
import boogie.ast.statement.*;
import ch.ethz.ameri.main.Main;
import whyML.ast.formula.*;
import whyML.ast.formula.quantifier.FormulaBinder;
import whyML.ast.formula.quantifier.QuantifiedFormula;
import whyML.ast.terms.SymbolTerm;
import whyML.ast.terms.Term;
import whyML.ast.types.Type;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;


public class ConstantAxiomGenerator implements ASTVisitor {

    ConstantDeclarationCollector cdc;
    int axiomCounter = 0;

    private String nextAxiomName(){
        return "C"+(axiomCounter++);
    }

    public ConstantAxiomGenerator(ProgramFactory programFactory){
        cdc = new ConstantDeclarationCollector(programFactory);
        generateUniquenessAxioms();
        for(Declaration d: programFactory.getASTRoot().getDeclarations()){
            d.accept(this);
        }
    }


    /**
     * axioms for unique constants
     * e.g.: axiom C1:a <> b && a <> c && b <> c
     */
    private void generateUniquenessAxioms(){
        for(Map.Entry<ASTType,List<String>> entry: cdc.getUniqueConstatDeclarations().entrySet()){
            List<String> constantNames = entry.getValue();
            if(!Main.disableCC && constantNames.size() >= 100){
                int n = constantNames.size();
                long axiomCount = (n*(n-1))/2;
                throw new RuntimeException("Error: there are "+ constantNames.size() + " unique constants of " +
                        "type \""+entry.getKey()+ "\" which would lead to " +axiomCount+" axioms being generated. ");
            }
            for(int i = 0; i < constantNames.size(); ++i){
                for(int j = i+1; j < constantNames.size();++j){
                    Term leftTerm = new SymbolTerm(constantNames.get(i));
                    Term rightTerm = new SymbolTerm(constantNames.get(j));
                    Formula formula = new InfixFormula(leftTerm,"<>",rightTerm);
                    whyML.ast.declerations.Axiom axiom = new whyML.ast.declerations.Axiom(nextAxiomName(),formula);
                    generatedAxioms.add(axiom);
                }
            }
        }
    }


    /**
     * create axioms of the form axiom A1: forall a:T. (a <>c) -> not c <: a
     * where ident is 'c' in the example above.
     * @param ident
     * @param type the WhyML type of c.
     * @return a list of whyML axioms, as described above.
     */
    public whyML.ast.declerations.Axiom generateEmptyParentEdgeAxiom(String ident, Type type){
        //axiom A1: forall a:T. (a <>c) -> not c <: a
        List<whyML.ast.declerations.Axiom> result = new LinkedList<>();
        FormulaBinder formulaBinder = new FormulaBinder(var1,type);
        InfixFormula implicator = new InfixFormula(new SymbolTerm(ident), inequalitySymbol,new SymbolTerm(var1));
        Formula implicatee = new NegationFormula(new InfixFormula(new SymbolTerm(ident),partialOrder, new SymbolTerm(var1)));
        BinaryFormula implicationFormula = new BinaryFormula(implicator, implicationSymbol, implicatee);
        QuantifiedFormula axiomFormula = new QuantifiedFormula(true, formulaBinder, implicationFormula);
        whyML.ast.declerations.Axiom parentEdgeEmptyAxiom = new whyML.ast.declerations.Axiom(nextAxiomName(),axiomFormula);
        return parentEdgeEmptyAxiom;
    }

    //TODO put these in new class as statics.
    private String inequalitySymbol = "<>";
    private String partialOrder = "<:";
    private String implicationSymbol = "->";
    private String equalitySymbol = "=";
    private String orSymbol = "||";

    /**
     * axiom A1 : forall t : T. (c <: t) -> (c=t || p_1 < : t || … || p_n < : t)
     * @param childIdent c
     * @param parents p_1 ... p_n (must contain at least 1).
     * @param type T
     * @return
     */
    public whyML.ast.declerations.Axiom generateFullParentEdgeAxiom(String childIdent, List<String> parents, Type type){
        String boundVariable = var1;
        Formula implicator = new InfixFormula(new SymbolTerm(childIdent),partialOrder,new SymbolTerm(boundVariable));
        List<Formula> implicateeList = new LinkedList<>();
        implicateeList.add(new InfixFormula(new SymbolTerm(childIdent),equalitySymbol,new SymbolTerm(boundVariable)));
        for(String parentIdent: parents){
            implicateeList.add(new InfixFormula(new SymbolTerm(parentIdent), partialOrder, new SymbolTerm(boundVariable)));
        }
        Formula implicatee = mergeFormulas(implicateeList,orSymbol);
        FormulaBinder binder = new FormulaBinder(boundVariable,type);
        Formula implicationFormula = new BinaryFormula(implicator,implicationSymbol,implicatee);
        QuantifiedFormula axiomFormula = new QuantifiedFormula(true,binder,implicationFormula);
        return new whyML.ast.declerations.Axiom(nextAxiomName(),axiomFormula);
    }


    /**
     * if formulas is null or empty, return null. otherwise return a new formula consisting of all formulas, separated by the given symbol.
     * e.g. (((f1 && f2) && f3) ....) (if separatorSymbol is &&)
     * @param separatorSymbol
     * @return
     */
    public static Formula mergeFormulas(List<Formula> formulas, String separatorSymbol){
        if(formulas == null || formulas.isEmpty()){
            return null;
        }
        Formula result = formulas.get(0);

        for(int i = 1; i < formulas.size(); ++i){
            result = new BinaryFormula(result,separatorSymbol,formulas.get(i));
        }
        return result;
    }

    //this class uses 5 underscores as prefix. (doesn't clash with others)
    //todo but it would be better to put all these prefixes together as statics.
    private String prefix = "_____";
    private String var1 = prefix+"a";
    private String var2 = prefix+"b";
    private String var3 = prefix+"c";



    private List<whyML.ast.declerations.Axiom> generatedAxioms = new LinkedList<>();

    public List<whyML.ast.declerations.Axiom> getGeneratedAxioms() {
        return generatedAxioms;
    }

    @Override
    public void visit(BitvecLiteral bitvecLiteral) {

    }

    @Override
    public void visit(BooleanLiteral booleanLiteral) {

    }

    @Override
    public void visit(IntegerLiteral integerLiteral) {

    }

    @Override
    public void visit(RealLiteral realLiteral) {

    }

    @Override
    public void visit(StringLiteral stringLiteral) {

    }

    @Override
    public void visit(ArrayAccessExpression arrayAccessExpression) {

    }

    @Override
    public void visit(ArrayStoreExpression arrayStoreExpression) {

    }

    @Override
    public void visit(BinaryExpression binaryExpression) {

    }

    @Override
    public void visit(BitVectorAccessExpression bitVectorAccessExpression) {

    }

    @Override
    public void visit(CodeExpression codeExpression) {

    }

    @Override
    public void visit(FunctionApplication functionApplication) {

    }

    @Override
    public void visit(FunctionApplicationWithType functionApplicationWithType) {

    }

    @Override
    public void visit(IdentifierExpression identifierExpression) {

    }

    @Override
    public void visit(IfThenElseExpression ifThenElseExpression) {

    }

    @Override
    public void visit(QuantifierExpression quantifierExpression) {

    }

    @Override
    public void visit(LambdaExpression lambdaExpression) {

    }

    @Override
    public void visit(UnaryExpression unaryExpression) {

    }

    @Override
    public void visit(WildcardExpression wildcardExpression) {

    }

    @Override
    public void visit(EnsuresSpecification ensuresSpecification) {

    }

    @Override
    public void visit(LoopInvariantSpecification loopInvariantSpecification) {

    }

    @Override
    public void visit(ModifiesSpecification modifiesSpecification) {

    }

    @Override
    public void visit(RequiresSpecification requiresSpecification) {

    }

    @Override
    public void visit(AssertStatement assertStatement) {

    }

    @Override
    public void visit(AssignmentStatement assignmentStatement) {

    }

    @Override
    public void visit(AssumeStatement assumeStatement) {

    }

    @Override
    public void visit(BreakStatement breakStatement) {

    }

    @Override
    public void visit(CallStatement callStatement) {

    }

    @Override
    public void visit(GotoStatement gotoStatement) {

    }

    @Override
    public void visit(HavocStatement havocStatement) {

    }

    @Override
    public void visit(IfStatement ifStatement) {

    }

    @Override
    public void visit(Label label) {

    }

    @Override
    public void visit(ParallelCall parallelCall) {

    }

    @Override
    public void visit(ReturnStatement returnStatement) {

    }

    @Override
    public void visit(WhileStatement whileStatement) {

    }

    @Override
    public void visit(YieldStatement yieldStatement) {

    }

    @Override
    public void visit(ArrayLHS arrayLHS) {

    }

    @Override
    public void visit(Body body) {

    }

    @Override
    public void visit(NamedAttribute namedAttribute) {

    }

    @Override
    public void visit(ParentEdge parentEdge) {

    }

    @Override
    public void visit(Project project) {

    }

    @Override
    public void visit(Trigger trigger) {

    }

    @Override
    public void visit(Unit unit) {

    }

    @Override
    public void visit(VariableLHS variableLHS) {

    }

    @Override
    public void visit(VarList varList) {

    }

    @Override
    public void visit(Axiom axiom) {

    }


    @Override
    public void visit(ConstDeclaration constDeclaration) {

    }


    @Override
    public void visit(FunctionDeclaration functionDeclaration) {

    }

    @Override
    public void visit(Implementation implementation) {

    }

    @Override
    public void visit(ProcedureDeclaration procedureDeclaration) {

    }

    @Override
    public void visit(TypeDeclaration typeDeclaration) {

    }

    @Override
    public void visit(VariableDeclaration variableDeclaration) {

    }

    @Override
    public void visit(ArrayAstType arrayAstType) {

    }

    @Override
    public void visit(NamedAstType namedAstType) {

    }

    @Override
    public void visit(PrimitiveAstType primitiveAstType) {

    }
}
