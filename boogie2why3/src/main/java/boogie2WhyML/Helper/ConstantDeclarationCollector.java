package boogie2WhyML.Helper;

import boogie.ProgramFactory;
import boogie.ast.*;
import boogie.ast.asttypes.ASTType;
import boogie.ast.asttypes.ArrayAstType;
import boogie.ast.asttypes.NamedAstType;
import boogie.ast.asttypes.PrimitiveAstType;
import boogie.ast.astvisitor.ASTVisitor;
import boogie.ast.declaration.*;
import boogie.ast.expression.*;
import boogie.ast.expression.literal.*;
import boogie.ast.specification.EnsuresSpecification;
import boogie.ast.specification.LoopInvariantSpecification;
import boogie.ast.specification.ModifiesSpecification;
import boogie.ast.specification.RequiresSpecification;
import boogie.ast.statement.*;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * this is a helper for ConstantAxiomGenerator.It traverses the Boogie AST once to collect information which is needed
 * for ConstantAxiomGenerator. It basically gives ConstantAxiomGenerator a full view over all declared constants, instead
 * of just the one it is currently processing
 */
public class ConstantDeclarationCollector implements ASTVisitor {

    /**
     * maps from a type name, to a list of all declared constants of this type
     */
    private Map<ASTType,List<String>> constatDeclarations = new HashMap<>();

    /**
     * maps from a type name, to a list of all declared UNIQUE constants of this type
     */
    private Map<ASTType,List<String>> uniqueConstatDeclarations = new HashMap<>();

    /**
     * map from a name of a constant, to a list of all its declared children.
     * for COMPLETE keyword
     */
    private Map<String, List<String>> children = new HashMap<>();


    /**
     * map from a constant declaration name, to a list of all constant delcarations (of the same type) which declare this
     * constant declaration as its UNIQUE PARENT.
     */
    private Map<String, List<String>> childrenOfUniqueParent = new HashMap<>();


    protected ConstantDeclarationCollector(ProgramFactory programFactory){
        for(Declaration d: programFactory.getASTRoot().getDeclarations()){
            d.accept(this);
        }
    }

    public Map<ASTType, List<String>> getConstatDeclarations() {
        return constatDeclarations;
    }

    public Map<ASTType, List<String>> getUniqueConstatDeclarations() {
        return uniqueConstatDeclarations;
    }

    public Map<String, List<String>> getChildren() {
        return children;
    }

    public Map<String, List<String>> getChildrenOfUniqueParent() {
        return childrenOfUniqueParent;
    }

    @Override
    public void visit(BitvecLiteral bitvecLiteral) {

    }

    @Override
    public void visit(BooleanLiteral booleanLiteral) {

    }

    @Override
    public void visit(IntegerLiteral integerLiteral) {

    }

    @Override
    public void visit(RealLiteral realLiteral) {

    }

    @Override
    public void visit(StringLiteral stringLiteral) {

    }

    @Override
    public void visit(ArrayAccessExpression arrayAccessExpression) {

    }

    @Override
    public void visit(ArrayStoreExpression arrayStoreExpression) {

    }

    @Override
    public void visit(BinaryExpression binaryExpression) {

    }

    @Override
    public void visit(BitVectorAccessExpression bitVectorAccessExpression) {

    }

    @Override
    public void visit(CodeExpression codeExpression) {

    }

    @Override
    public void visit(FunctionApplication functionApplication) {

    }

    @Override
    public void visit(FunctionApplicationWithType functionApplicationWithType) {

    }

    @Override
    public void visit(IdentifierExpression identifierExpression) {

    }

    @Override
    public void visit(IfThenElseExpression ifThenElseExpression) {

    }

    @Override
    public void visit(QuantifierExpression quantifierExpression) {

    }

    @Override
    public void visit(LambdaExpression lambdaExpression) {

    }

    @Override
    public void visit(UnaryExpression unaryExpression) {

    }

    @Override
    public void visit(WildcardExpression wildcardExpression) {

    }

    @Override
    public void visit(EnsuresSpecification ensuresSpecification) {

    }

    @Override
    public void visit(LoopInvariantSpecification loopInvariantSpecification) {

    }

    @Override
    public void visit(ModifiesSpecification modifiesSpecification) {

    }

    @Override
    public void visit(RequiresSpecification requiresSpecification) {

    }

    @Override
    public void visit(AssertStatement assertStatement) {

    }

    @Override
    public void visit(AssignmentStatement assignmentStatement) {

    }

    @Override
    public void visit(AssumeStatement assumeStatement) {

    }

    @Override
    public void visit(BreakStatement breakStatement) {

    }

    @Override
    public void visit(CallStatement callStatement) {

    }

    @Override
    public void visit(GotoStatement gotoStatement) {

    }

    @Override
    public void visit(HavocStatement havocStatement) {

    }

    @Override
    public void visit(IfStatement ifStatement) {

    }

    @Override
    public void visit(Label label) {

    }

    @Override
    public void visit(ParallelCall parallelCall) {

    }

    @Override
    public void visit(ReturnStatement returnStatement) {

    }

    @Override
    public void visit(WhileStatement whileStatement) {

    }

    @Override
    public void visit(YieldStatement yieldStatement) {

    }

    @Override
    public void visit(ArrayLHS arrayLHS) {

    }

    @Override
    public void visit(Body body) {

    }

    @Override
    public void visit(NamedAttribute namedAttribute) {

    }

    @Override
    public void visit(ParentEdge parentEdge) {

    }

    @Override
    public void visit(Project project) {

    }

    @Override
    public void visit(Trigger trigger) {

    }

    @Override
    public void visit(Unit unit) {

    }

    @Override
    public void visit(VariableLHS variableLHS) {

    }

    @Override
    public void visit(VarList varList) {

    }

    @Override
    public void visit(Axiom axiom) {

    }

    @Override
    public void visit(ConstDeclaration constDeclaration){
        if(constatDeclarations.get(constDeclaration.getVarList().getType()) == null){
            constatDeclarations.put(constDeclaration.getVarList().getType(),new LinkedList<>());
        }
        if(uniqueConstatDeclarations.get(constDeclaration.getVarList().getType()) == null){
            uniqueConstatDeclarations.put(constDeclaration.getVarList().getType(),new LinkedList<>());
        }
        for(String s: constDeclaration.getVarList().getIdentifiers()){
            constatDeclarations.get(constDeclaration.getVarList().getType()).add(s);
            if(constDeclaration.isUnique()){
                uniqueConstatDeclarations.get(constDeclaration.getVarList().getType()).add(s);
            }
        }

        if(constDeclaration.getParentInfo() != null){
            List<String> parents = new LinkedList<>();
            for(ParentEdge parentEdge : constDeclaration.getParentInfo()){
                parents.add(parentEdge.getIdentifier());
            }
            for(int i = 0; i < parents.size(); ++i){
                String parentName = parents.get(i);
                if(children.get(parentName) == null){
                    children.put(parentName,new LinkedList<>());
                }
                for(String childName: constDeclaration.getVarList().getIdentifiers()){
                    children.get(parentName).add(childName);
                }
                if(constDeclaration.getParentInfo()[i].isUnique()){
                    if(childrenOfUniqueParent.get(parentName) == null){
                        childrenOfUniqueParent.put(parentName, new LinkedList<>());
                    }
                    for(String childName: constDeclaration.getVarList().getIdentifiers()){
                        childrenOfUniqueParent.get(parentName).add(childName);
                    }
                }
            }
        }

    }

    @Override
    public void visit(FunctionDeclaration functionDeclaration) {

    }

    @Override
    public void visit(Implementation implementation) {

    }

    @Override
    public void visit(ProcedureDeclaration procedureDeclaration) {

    }

    @Override
    public void visit(TypeDeclaration typeDeclaration) {

    }

    @Override
    public void visit(VariableDeclaration variableDeclaration) {

    }

    @Override
    public void visit(ArrayAstType arrayAstType) {

    }

    @Override
    public void visit(NamedAstType namedAstType) {

    }

    @Override
    public void visit(PrimitiveAstType primitiveAstType) {

    }
}
