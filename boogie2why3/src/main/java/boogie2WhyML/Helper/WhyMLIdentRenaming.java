package boogie2WhyML.Helper;

import whyML.ast.ASTNode;
import whyML.ast.FormulaCase;
import whyML.ast.Label;
import whyML.ast.Param;
import whyML.ast.declerations.*;
import whyML.ast.declerations.letdeclaration.*;
import whyML.ast.expression.*;
import whyML.ast.formula.*;
import whyML.ast.formula.quantifier.FormulaBinder;
import whyML.ast.formula.quantifier.QuantifiedFormula;
import whyML.ast.formula.quantifier.Trigger;
import whyML.ast.pattern.TuplePattern;
import whyML.ast.pattern.VariablePattern;
import whyML.ast.spec.*;
import whyML.ast.terms.*;
import whyML.ast.types.EmptyTupleType;
import whyML.ast.types.TypeSymbol;
import whyML.ast.types.TypeTuple;
import whyML.ast.types.TypeVariable;
import whyML.astvisitor.ASTVisitor;

import java.util.*;


/**
 * currently, this class is not used anymore, as it does not work with our translation method. Instead, BoogieIdentRenaming is used.
 */
public class WhyMLIdentRenaming implements ASTVisitor {

    public WhyMLIdentRenaming(){

    }

    public void renameAccordingToWhyMLSyntax(List<Declaration> declarations){
        for(Declaration d: declarations){
            d.accept(this);
        }
    }

    Set<ASTNode> processed = new HashSet<>();

    /*
    helper methods
     */

    private String lprefix = "__";


    private String uprefix = "";

    /**
     * replaces unsupported characters, and makes the identifier lower case.
     * @param input
     * @return
     */
    private String lRenameAndReplace(String input){
        if(noReplacingAllowed(input)){
            return input;
        }
        return lprefix +replaceUnsupportedChars(input);
    }

    private String uRenameAndReplace(String input) { return uprefix+replaceUnsupportedChars(input); }


    private boolean noReplacingAllowed(String s){
        return s.trim().equals("int") || s.trim().equals("bool") || s.trim().equals("map") || s.trim().equals("real")
                || s.contains("<") || s.trim().equals("get") || s.trim().equals("set" ) || s.trim().equals("ref")|| s.trim().equals("havoc")
                || s.trim().equals("contents")|| s.trim().equals("notb")|| s.trim().equals("true")|| s.trim().equals("false");
    }


    /**
     * replaces character which are allowed in Boogie, but not in WhyML, with some that are.
     * @param input
     * @return
     */
    private String replaceUnsupportedChars(String input){
        //TODO unicode characters...
        String res = input;
        //res = res.replace("_", "_US_");
        res = res.replace("#", "_PD_");
        res = res.replace("$","_DL_");
        //TODO when we create variable access, we translate it to x.contents (the dot is part of the expression)
        //so for now we dont replace dots.
        //res = res.replace(".","_DOT_");
        res = res.replace("?","_SQ_");
        res = res.replace("`","_BQ_");
        res = res.replace("~","_TD_");
        res = res.replace("^","_CT_");
        res = res.replace("\\","_BS_");
        res = res.replace("?","_QM_");
        return res;
    }


    private void processChildren(ASTNode node){
        for(ASTNode child: node.getChildren()){
            if(!processed.contains(child)){
                child.accept(this);
                processed.add(child);
            }

        }
    }


    @Override
    public void visit(Use use) {
        //DO NOTHING.
    }

    @Override
    public void visit(Axiom axiom) {
        axiom.setIdent(replaceUnsupportedChars(axiom.getIdent()));
        processChildren(axiom);
    }

    @Override
    public void visit(Lemma lemma) {
        lemma.setIdent(replaceUnsupportedChars(lemma.getIdent()));
        processChildren(lemma);
    }



    @Override
    public void visit(Comment comment) {
        //do nothing.
    }

    @Override
    public void visit(Empty empty) {
        //nothing.
    }

    @Override
    public void visit(ValGlobalVariable val) {
        val.setLident(lRenameAndReplace(val.getLident()));
        processChildren(val);
    }

    @Override
    public void visit(ValAbstractFunction val) {
        val.setName(lRenameAndReplace(val.getName()));
        processChildren(val);
    }

    @Override
    public void visit(Predicate predicate) {
        predicate.setName(lRenameAndReplace(predicate.getName()));
        processChildren(predicate);
    }

    @Override
    public void visit(TrueFormula t) {
        processChildren(t);
    }

    @Override
    public void visit(FalseFormula f) {
        processChildren(f);
    }

    @Override
    public void visit(QuantifiedFormula qf) {
        processChildren(qf);
    }

    @Override
    public void visit(IntegerTerm term) {
        processChildren(term);
    }

    @Override
    public void visit(SymbolTerm symbol) {
        symbol.setLqualid(lRenameAndReplace(symbol.getLqualid()));
        processChildren(symbol);
    }

    @Override
    public void visit(InfixFormula term) {
        processChildren(term);
    }

    @Override
    public void visit(BinaryFormula formula) {
        processChildren(formula);
    }

    @Override
    public void visit(ExceptionDeclaration ed) {
        ed.setUident(uRenameAndReplace(ed.getUident()));
        processChildren(ed);
    }

    @Override
    public void visit(Postcondition p) {
        processChildren(p);
    }

    @Override
    public void visit(Precondition p) {
        processChildren(p);
    }

    @Override
    public void visit(FunctionApplicationTerm fat) {
        fat.setLqualid(lRenameAndReplace(fat.getLqualid()));
        processChildren(fat);
    }

    @Override
    public void visit(FunctionApplicationWithTypeTerm fawtt) {
        fawtt.setLqualid(lRenameAndReplace(fawtt.getLqualid()));
        processChildren(fawtt);
    }

    @Override
    public void visit(Invariant inv) {
        processChildren(inv);
    }

    @Override
    public void visit(TypeDeclaration td) {
        td.setLident(lRenameAndReplace(td.getLident()));
        processChildren(td);
    }

    @Override
    public void visit(SymbolFormula formula) {
        formula.setLqualid(lRenameAndReplace(formula.getLqualid()));
    }

    @Override
    public void visit(FunctionDeclaration functionDeclaration) {
        functionDeclaration.setName(lRenameAndReplace(functionDeclaration.getName()));
        processChildren(functionDeclaration);
    }

    @Override
    public void visit(NegationFormula negationFormula) {
        processChildren(negationFormula);
    }

    @Override
    public void visit(InfixTerm infixTerm) {
        processChildren(infixTerm);
    }

    @Override
    public void visit(ConstantDeclaration constantDeclaration) {
        constantDeclaration.setLident(lRenameAndReplace(constantDeclaration.getLident()));
        processChildren(constantDeclaration);
    }

    @Override
    public void visit(RealTerm realTerm) {
        processChildren(realTerm);
    }

    @Override
    public void visit(VariablePattern variablePattern) {
        variablePattern.setLident(lRenameAndReplace(variablePattern.getLident()));
        processChildren(variablePattern);
    }

    @Override
    public void visit(TuplePattern tuplePattern) {
        processChildren(tuplePattern);
    }

    @Override
    public void visit(FormulaCase formulaCase) {
        processChildren(formulaCase);
    }

    @Override
    public void visit(ReturnsSpecification returnsSpecification) {
        processChildren(returnsSpecification);
    }

    @Override
    public void visit(SymbolExpression symbolExpression) {
        symbolExpression.setLqualid(lRenameAndReplace(symbolExpression.getLqualid()));
        processChildren(symbolExpression);
    }

    @Override
    public void visit(AnyExpression anyExpression) {
        throw new RuntimeException("Error: replace identifiers in any expression");
    }

    @Override
    public void visit(AssertionExpression assertionExpression) {
        processChildren(assertionExpression);
    }

    @Override
    public void visit(SequenceExpression sequenceExpression) {
        processChildren(sequenceExpression);
    }

    @Override
    public void visit(LocalBindingExpression localBindingExpression) {
        processChildren(localBindingExpression);
    }

    @Override
    public void visit(LetDeclarationBinder letDeclarationBinder) {
        if(!letDeclarationBinder.getIsParam()){
            letDeclarationBinder.setLident(lRenameAndReplace(letDeclarationBinder.getLident()));
        }
        processChildren(letDeclarationBinder);
    }

    @Override
    public void visit(FunBody funBody) {
        processChildren(funBody);
    }

    @Override
    public void visit(FunDefn funDefn) {
        funDefn.setLident(lRenameAndReplace(funDefn.getLident()));
        processChildren(funDefn);
    }

    @Override
    public void visit(RecDefn recDefn) {
        processChildren(recDefn);
    }

    @Override
    public void visit(RecursiveLetDeclaration recursiveLetDeclaration) {
        processChildren(recursiveLetDeclaration);
    }

    @Override
    public void visit(LetDeclaration letDeclaration) {
        letDeclaration.setLident(lRenameAndReplace(letDeclaration.getLident()));
        processChildren(letDeclaration);
    }

    @Override
    public void visit(Param param) {
        param.setName(lRenameAndReplace(param.getName()));
        processChildren(param);
    }

    @Override
    public void visit(IntegerExpression integerExpression) {
        processChildren(integerExpression);
    }

    @Override
    public void visit(RealExpression realExpression) {
        processChildren(realExpression);
    }

    @Override
    public void visit(ConditionalExpression conditionalExpression) {
        processChildren(conditionalExpression);
    }

    @Override
    public void visit(Handler handler) {
        handler.setUqualid(uRenameAndReplace(handler.getUqualid()));
        processChildren(handler);
    }

    @Override
    public void visit(ExceptionCatchingExpression expression) {
        processChildren(expression);
    }

    @Override
    public void visit(ExceptionRaisingExpression raisingExpression) {
        raisingExpression.setUqualid(uRenameAndReplace(raisingExpression.getUqualid()));
        processChildren(raisingExpression);
    }

    @Override
    public void visit(WritesSpec spec) {
        processChildren(spec);
    }

    @Override
    public void visit(WhileLoopExpression expression) {
        processChildren(expression);
    }

    @Override
    public void visit(Variant variant) {
        processChildren(variant);
    }

    @Override
    public void visit(FunctionApplicationExpression expression) {
        processChildren(expression);
    }

    @Override
    public void visit(FunctionApplicationWithTypeExpression expression) {
        processChildren(expression);
    }

    @Override
    public void visit(TupleExpression tupleExpression) {
        processChildren(tupleExpression);
    }

    @Override
    public void visit(CodeMarkExpression c) {
        processChildren(c);
    }

    @Override
    public void visit(TupleTerm tupleTerm) {
        processChildren(tupleTerm);
    }

    @Override
    public void visit(FieldAssignmentExpression fae) {
        if(fae.getLqualid() != null){
            fae.setLqualid(lRenameAndReplace(fae.getLqualid()));
        }
        processChildren(fae);
    }

    @Override
    public void visit(InfixExpression expression) {
        processChildren(expression);
    }

    @Override
    public void visit(PrefixTerm prefixTerm) {
        processChildren(prefixTerm);
    }

    @Override
    public void visit(PrefixExpression prefixExpression) {
        processChildren(prefixExpression);
    }

    @Override
    public void visit(OldTerm oldTerm) {
        processChildren(oldTerm);
    }

    @Override
    public void visit(AtTerm atTerm) {
        processChildren(atTerm);
    }

    @Override
    public void visit(Trigger trigger) {
        processChildren(trigger);
    }

    @Override
    public void visit(PredicateApplicationFormula predicateApplication) {
        predicateApplication.setLqualid(lRenameAndReplace(predicateApplication.getLqualid()));
        processChildren(predicateApplication);
    }

    @Override
    public void visit(FormulaBinder formulaBinder) {
        List<String> oldIdents = formulaBinder.getIdents();
        List<String> newIdents = new LinkedList<>();
        for(String s : oldIdents){
            newIdents.add(lRenameAndReplace(s));
        }
        formulaBinder.setIdents(newIdents);
        processChildren(formulaBinder);
    }

    @Override
    public void visit(Label label) {
        processChildren(label);
    }

    @Override
    public void visit(TypeSymbol typeSymbol) {
        typeSymbol.setLqualid(lRenameAndReplace(typeSymbol.getLqualid()));
        processChildren(typeSymbol);
    }

    @Override
    public void visit(TypeTuple typeTuple) {
        processChildren(typeTuple);
    }

    @Override
    public void visit(TypeVariable typeVariable) {
        typeVariable.setLqualid(lRenameAndReplace(typeVariable.getLqualid()));
        processChildren(typeVariable);
    }

    @Override
    public void visit(EmptyTupleType type) {
        processChildren(type);
    }
}
