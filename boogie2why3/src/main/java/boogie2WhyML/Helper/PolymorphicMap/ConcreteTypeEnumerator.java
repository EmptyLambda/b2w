package boogie2WhyML.Helper.PolymorphicMap;

import boogie.ProgramFactory;
import boogie.ast.*;
import boogie.ast.asttypes.ArrayAstType;
import boogie.ast.asttypes.NamedAstType;
import boogie.ast.asttypes.PrimitiveAstType;
import boogie.ast.astvisitor.ASTVisitor;
import boogie.ast.declaration.*;
import boogie.ast.expression.*;
import boogie.ast.expression.literal.*;
import boogie.ast.specification.EnsuresSpecification;
import boogie.ast.specification.LoopInvariantSpecification;
import boogie.ast.specification.ModifiesSpecification;
import boogie.ast.specification.RequiresSpecification;
import boogie.ast.statement.*;
import boogie.type.BoogieType;
import boogie.type.PrimitiveType;

import java.util.*;


public class ConcreteTypeEnumerator implements ASTVisitor{


    /**
     * stores all possible concrete types in a set.
     * This includes: input/output arguments, variables, constants, expressions etc.
     * @param declarations
     */
    public ConcreteTypeEnumerator(Declaration[] declarations){
        this.allConcreteTypes = new HashSet<>();

        for(Declaration d: declarations){
            d.accept(this);
        }
    }

    private Set<BoogieType> allConcreteTypes;

    /**
     * add the underlying type to allConcreteTypes.
     * @param boogieType
     */
    private void addType(BoogieType boogieType){
        //TODO remove
        if(boogieType instanceof PrimitiveType){
            PrimitiveType pt = (PrimitiveType)boogieType;
            if (pt == PrimitiveType.errorType){
                pt = null;
            }
        }
        //TODO REMOVE
        if(boogieType == null){
            boogieType = null;
        }
        allConcreteTypes.add(boogieType.getUnderlyingType());
    }

    public Set<BoogieType> getAllConcreteTypes() {
        return allConcreteTypes;
    }

    //TODO count function declarations with type params, and how many type params they have.


    private Set<ASTNode> processed = new HashSet<>();

    /**
     * process children of an AST node.
     * @param objects
     */
    private void process(Collection<Object> objects){
        for(Object o: objects){
            if(o instanceof ASTNode){
                ASTNode node = (ASTNode)o;
                if(!processed.contains(node)){
                    node.accept(this);
                    processed.add(node);
                }
            }else if(o instanceof Collection){
                process((Collection)o);
            }else if(o instanceof Object[]){
                //(Changes to the returned list "write through" to the array according to javadoc, so this works)
                process(Arrays.asList((Object[]) o));
            }
        }
    }


    @Override
    public void visit(BitvecLiteral bitvecLiteral) {
        addType(bitvecLiteral.getType());
        process(bitvecLiteral.getChildren());
    }

    @Override
    public void visit(BooleanLiteral booleanLiteral) {
        addType(booleanLiteral.getType());
        process(booleanLiteral.getChildren());
    }

    @Override
    public void visit(IntegerLiteral integerLiteral) {
        addType(integerLiteral.getType());
        process(integerLiteral.getChildren());
    }

    @Override
    public void visit(RealLiteral realLiteral) {
        addType(realLiteral.getType());
        process(realLiteral.getChildren());
    }

    @Override
    public void visit(StringLiteral stringLiteral) {
        addType(stringLiteral.getType());
        process(stringLiteral.getChildren());
    }

    @Override
    public void visit(ArrayAccessExpression arrayAccessExpression) {
        addType(arrayAccessExpression.getType());
        process(arrayAccessExpression.getChildren());
    }

    @Override
    public void visit(ArrayStoreExpression arrayStoreExpression) {
        addType(arrayStoreExpression.getType());
        process(arrayStoreExpression.getChildren());
    }

    @Override
    public void visit(BinaryExpression binaryExpression) {
        addType(binaryExpression.getType());
        process(binaryExpression.getChildren());
    }

    @Override
    public void visit(BitVectorAccessExpression bitVectorAccessExpression) {
        addType(bitVectorAccessExpression.getType());
        process(bitVectorAccessExpression.getChildren());
    }

    @Override
    public void visit(CodeExpression codeExpression) {
        addType(codeExpression.getType());
        process(codeExpression.getChildren());
    }

    @Override
    public void visit(FunctionApplication functionApplication) {
        addType(functionApplication.getType());
        process(functionApplication.getChildren());
    }

    @Override
    public void visit(FunctionApplicationWithType functionApplicationWithType) {
        addType(functionApplicationWithType.getType());
        process(functionApplicationWithType.getChildren());
    }

    @Override
    public void visit(IdentifierExpression identifierExpression) {
        addType(identifierExpression.getType());
        process(identifierExpression.getChildren());
    }

    @Override
    public void visit(IfThenElseExpression ifThenElseExpression) {
        addType(ifThenElseExpression.getType());
        process(ifThenElseExpression.getChildren());
    }

    @Override
    public void visit(QuantifierExpression quantifierExpression) {
        addType(quantifierExpression.getType());
        process(quantifierExpression.getChildren());
    }

    @Override
    public void visit(LambdaExpression lambdaExpression) {
        addType(lambdaExpression.getType());
        process(lambdaExpression.getChildren());
    }

    @Override
    public void visit(UnaryExpression unaryExpression) {
        addType(unaryExpression.getType());
        process(unaryExpression.getChildren());
    }

    @Override
    public void visit(WildcardExpression wildcardExpression) {
        //TODO this can be ignored right? (it seems to be null at least some times)
        //addType(wildcardExpression.getType());
        process(wildcardExpression.getChildren());
    }

    @Override
    public void visit(EnsuresSpecification ensuresSpecification) {
        process(ensuresSpecification.getChildren());
    }

    @Override
    public void visit(LoopInvariantSpecification loopInvariantSpecification) {
        process(loopInvariantSpecification.getChildren());
    }

    @Override
    public void visit(ModifiesSpecification modifiesSpecification) {
        process(modifiesSpecification.getChildren());
    }

    @Override
    public void visit(RequiresSpecification requiresSpecification) {
        process(requiresSpecification.getChildren());
    }

    @Override
    public void visit(AssertStatement assertStatement) {
        process(assertStatement.getChildren());
    }

    @Override
    public void visit(AssignmentStatement assignmentStatement) {
        process(assignmentStatement.getChildren());
    }

    @Override
    public void visit(AssumeStatement assumeStatement) {
        process(assumeStatement.getChildren());
    }

    @Override
    public void visit(BreakStatement breakStatement) {
        process(breakStatement.getChildren());
    }

    @Override
    public void visit(CallStatement callStatement) {
        process(callStatement.getChildren());
    }

    @Override
    public void visit(GotoStatement gotoStatement) {
        process(gotoStatement.getChildren());
    }

    @Override
    public void visit(HavocStatement havocStatement) {
        process(havocStatement.getChildren());
    }

    @Override
    public void visit(IfStatement ifStatement) {
        process(ifStatement.getChildren());
    }

    @Override
    public void visit(Label label) {
        process(label.getChildren());
    }

    @Override
    public void visit(ParallelCall parallelCall) {
        process(parallelCall.getChildren());
    }

    @Override
    public void visit(ReturnStatement returnStatement) {
        process(returnStatement.getChildren());
    }

    @Override
    public void visit(WhileStatement whileStatement) {
        process(whileStatement.getChildren());
    }

    @Override
    public void visit(YieldStatement yieldStatement) {
        process(yieldStatement.getChildren());
    }

    @Override
    public void visit(ArrayLHS arrayLHS) {
        addType(arrayLHS.getType());
        process(arrayLHS.getChildren());
    }

    @Override
    public void visit(Body body) {
        process(body.getChildren());
    }

    @Override
    public void visit(NamedAttribute namedAttribute) {
        process(namedAttribute.getChildren());
    }

    @Override
    public void visit(ParentEdge parentEdge) {
        process(parentEdge.getChildren());
    }

    @Override
    public void visit(Project project) {
        process(project.getChildren());
    }

    @Override
    public void visit(Trigger trigger) {
        process(trigger.getChildren());
    }

    @Override
    public void visit(Unit unit) {
        process(unit.getChildren());
    }

    @Override
    public void visit(VariableLHS variableLHS) {
        addType(variableLHS.getType());
        process(variableLHS.getChildren());
    }

    @Override
    public void visit(VarList varList) {
        //part of the children
        //addType(varList.getType().getBoogieType());
        process(varList.getChildren());
    }

    @Override
    public void visit(Axiom axiom) {
        process(axiom.getChildren());
    }

    @Override
    public void visit(ConstDeclaration constDeclaration) {
        process(constDeclaration.getChildren());
    }

    @Override
    public void visit(FunctionDeclaration functionDeclaration) {
        process(functionDeclaration.getChildren());
    }

    @Override
    public void visit(Implementation implementation) {
        process(implementation.getChildren());
    }

    @Override
    public void visit(ProcedureDeclaration procedureDeclaration) {
        process(procedureDeclaration.getChildren());
    }

    @Override
    public void visit(TypeDeclaration typeDeclaration) {
        process(typeDeclaration.getChildren());
    }

    @Override
    public void visit(VariableDeclaration variableDeclaration) {
        process(variableDeclaration.getChildren());
    }

    @Override
    public void visit(ArrayAstType arrayAstType) {
        addType(arrayAstType.getBoogieType());
        process(arrayAstType.getChildren());
    }

    @Override
    public void visit(NamedAstType namedAstType) {
        addType(namedAstType.getBoogieType());
        process(namedAstType.getChildren());
    }

    @Override
    public void visit(PrimitiveAstType primitiveAstType) {
        addType(primitiveAstType.getBoogieType());
        process(primitiveAstType.getChildren());
    }
}
