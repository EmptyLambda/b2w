package boogie2WhyML;

/**
 * Created by Michael on 12/28/2015.
 */
public enum  ErrorMessages {
    GOTO_WARNING("'goto' statement not supported, replaced by 'assert false;'"),
    BREAK_TO_LABEL("breaking to label not supported yet, replaced by 'assert false;'"),
    ARRAY_ACCESS_EXPRESSION("array access expression using parametric type might be rewritten into \n"+
                                    "concrete types. The type parameter comes from a quantifier or a procedure."),
    ARRAY_STORE_EXPRESSION("array store expression using parametric type might be rewritten into \n"+
            "concrete types. The type parameter comes from a quantifier or a procedure."),
    LAMBDA_EXPRESSION("lambda expression contains type parameters." +
            "Translation is not guaranteed to be executable. If it is executable, this warning can be ignored."),
    IF_THEN_ELSE_ASSIGNMENT("'if then else' expressions within assignments are not supported yet.")
    ;
    private final String text;

    private ErrorMessages(String text){
        this.text = text;
    }

    public String getWarning(){
        return "Warning: "+text;
    }

}
