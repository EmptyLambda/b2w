package ch.ethz.ameri.main;

import com.beust.jcommander.Parameter;

import java.util.ArrayList;
import java.util.List;

/**
 * used to parse input arguments.
 */
public class JCommanderInput {

    @Parameter(names = "-debug", description = "Enable debug mode")
    public boolean debug = false;

    @Parameter(description = "inputFile [outputFile]")
    public List<String> files = new ArrayList<>();

    @Parameter(names = { "-h", "-help" }, help = true, description = "Display usage")
    public boolean help;

    @Parameter(names = "-transform", description = "File name for transformed Boogie code.")
    public String transformFileName;

    @Parameter(names = "-module", description = "Optional Module name. Must begin with Uppercase letter.")
    public String moduleName;

    @Parameter(names = "-overwrite", description = "If this flag is set, generated files will overwrite existing ones. " +
            "Otherwise text is appended if a file exists.")
    public boolean overwrite = false;

    @Parameter(names = "-disableCC", description = "If this flag is NOT set, the program exits if there are more than 100" +
            " 'unique' constants of a single type.")
    public boolean disableCC = false;
}
