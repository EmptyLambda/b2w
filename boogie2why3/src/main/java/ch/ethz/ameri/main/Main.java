package ch.ethz.ameri.main;

import boogie.ProgramFactory;
import boogie2WhyML.BoogieTranslator;
import boogie2WhyML.PreambleGenerator;
import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import org.apache.log4j.Level;
import util.Log;
import whyML.ast.Module;
import whyML.ast.declerations.Declaration;
import whyML.astvisitor.ModulePrinterVisitor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {



    public static boolean disableCC = false;


    public static void main(String[] args) {

        JCommanderInput jci = new JCommanderInput();
        JCommander jCommander = new JCommander(jci,args);
        if(jci.help || jci.files.size() < 1 || jci.files.size() > 2){
            jCommander.usage();
            return;
        }
        if(!jci.debug){
            Log.v().setLevel(Level.OFF);
        }
        String boogieFileName = jci.files.get(0);
        String translationOutputFile = null;
        if(jci.files.size() == 2){
            translationOutputFile = jci.files.get(1);
        }
        String transformFile = jci.transformFileName;

        String moduleName = "Translation";
        if(jci.moduleName != null){
            moduleName = jci.moduleName;
        }
        disableCC = jci.disableCC;
        String moduleComment = "This translation was automatically generated.";


        try{
            //start actual translation
            ProgramFactory pf;
            try {
                pf = new ProgramFactory(boogieFileName);
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
            Log.info("Programfactory created.");
            Log.info("Translate declarations...");
            Module translationModule = new Module(moduleName, moduleComment);
            BoogieTranslator bt = new BoogieTranslator();
            //no renaming, it is done on the Boogie AST now.
            List<Declaration> translatedDeclarations = bt.translate(pf,true,false,true);
            translationModule.addDeclerations(translatedDeclarations);
            ModulePrinterVisitor mpvTranslation = new ModulePrinterVisitor(translationModule);
            List<String> printableModuleTranslation = mpvTranslation.printModuleToString();
            //print to file or console.
            ModulePrinterVisitor.print(printableModuleTranslation, translationOutputFile, jci.overwrite);
            Log.info("Translation Done.");
            if(transformFile != null){
                //ModulePrinterVisitor.createFile(transformFile);
                pf.toFile(transformFile);
            }
            if(bt.warningIssued){
                System.exit(5);
            }else{
                System.exit(0);
            }
        }catch(Exception e){
            System.err.println(boogieFileName+": "+e.toString());
            System.exit(10);
        }

    }


}
