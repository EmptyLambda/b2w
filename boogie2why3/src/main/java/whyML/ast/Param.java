package whyML.ast;


import whyML.ast.types.Type;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;

public class Param implements ASTNode{
    private String name;
    private Boolean isGhost;
    private Label label;
    private Type type;

    @Override
    public List<ASTNode> getChildren() {
        List<ASTNode> children = new LinkedList<>();
        children.add(label);
        children.add(type);
        return children;
    }

    public Param(String name, Boolean isGhost, Label label, Type type){
        this.name = name;
        this.isGhost = isGhost;
        this.label = label;
        this.type = type;
    }

    /**
     * Param constructor which is NOT a ghost, and has NO label
     * @param name
     * @param type
     */
    public Param(String name, Type type){
        this.name = name;
        this.isGhost = false;
        this.label = new Label(false);
        this.type = type;
    }


    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Boolean getIsGhost() {
        return isGhost;
    }

    public Label getLabel() {
        return label;
    }

    public Type getType() {
        return type;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }
}
