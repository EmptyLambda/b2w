package whyML.ast;

import whyML.ast.declerations.Declaration;

import java.util.LinkedList;
import java.util.List;


public class Module {

    public Module(String name, String comment){
        this.name = name;
        this.comment = comment;
        this.declarations = new LinkedList<Declaration>();
    }

    private String name;
    private String comment;

    private List<Declaration> declarations;

    public List<Declaration> getDeclarations() {
        return declarations;
    }

    public void addDecleration(Declaration d) {
        this.declarations.add(d);
    }

    public void addDeclerations(List<Declaration> declarations) {
        this.declarations.addAll(declarations);
    }

    public String getComment() {
        return comment;
    }

    public String getName() {
        return name;
    }
}
