package whyML.ast.terms;


import whyML.ast.ASTNode;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;

public class IntegerTerm extends Term {

    //string representation of the integers value. Allows arbitrary precision, and we don't need computation.
    private String value;

    public IntegerTerm(String value){
        this.value = value;
    }


    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public List<ASTNode> getChildren() {
        return new LinkedList<>();
    }

    public String getValue() {
        return value;
    }
}
