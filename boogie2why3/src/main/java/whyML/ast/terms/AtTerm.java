package whyML.ast.terms;

import whyML.ast.ASTNode;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;


public class AtTerm extends Term {

    private Term subTerm;
    private String uident;

    public AtTerm(Term subTerm, String uident) {
        this.subTerm = subTerm;
        this.uident = uident;
    }

    public String getUident() {
        return uident;
    }

    public Term getSubTerm() {
        return subTerm;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public List<ASTNode> getChildren() {
        List<ASTNode> children = new LinkedList<>();
        children.add(subTerm);
        return children;
    }
}
