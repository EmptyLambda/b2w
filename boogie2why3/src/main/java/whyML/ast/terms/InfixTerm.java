package whyML.ast.terms;


import whyML.ast.ASTNode;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;

public class InfixTerm extends Term{

    private String infixOperator;//TODO could be replaced by an enum.
    private Term leftTerm;
    private Term rightTerm;

    public InfixTerm(Term leftTerm, String infixOperator, Term rightTerm){
        this.leftTerm =leftTerm;
        this.infixOperator = infixOperator;
        this.rightTerm = rightTerm;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    public String getInfixOperator() {
        return infixOperator;
    }

    public Term getLeftTerm() {
        return leftTerm;
    }

    public Term getRightTerm() {
        return rightTerm;
    }

    @Override
    public List<ASTNode> getChildren() {
        List<ASTNode> children = new LinkedList<>();
        children.add(leftTerm);
        children.add(rightTerm);
        return children;
    }
}
