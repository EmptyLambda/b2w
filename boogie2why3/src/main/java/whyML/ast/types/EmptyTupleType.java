package whyML.ast.types;


import whyML.ast.ASTNode;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;

public class EmptyTupleType extends Type{

    boolean isRef;

    public EmptyTupleType(){
        isRef = false;
    }

    @Override
    public String getTypeName() {
        if(isRef){
            return " ref () ";
        }else{
            return " () ";
        }
    }

    @Override
    public void addRef() {
        isRef = true;
    }

    @Override
    public List<ASTNode> getChildren() {
        return new LinkedList<>();
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }
}
