package whyML.ast.expression;


import whyML.ast.ASTNode;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;

public class ExceptionRaisingExpression extends Expression{

    private String uqualid;

    public ExceptionRaisingExpression(String uqualid){
        this.uqualid = uqualid;
    }

    public String getUqualid() {
        return uqualid;
    }

    public void setUqualid(String uqualid) {
        this.uqualid = uqualid;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public List<ASTNode> getChildren() {
        return new LinkedList<>();
    }
}
