package whyML.ast.expression;


import whyML.ast.ASTNode;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;

public class CodeMarkExpression extends Expression {

    private String uident;
    private Expression expression;

    /**
     * 'uident: expression
     * @param uident
     * @param expression
     */
    public CodeMarkExpression(String uident, Expression expression) {
        this.uident = uident;
        this.expression = expression;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    public String getUident() {
        return uident;
    }

    public void setUident(String uident) {
        this.uident = uident;
    }

    public Expression getExpression() {
        return expression;
    }

    public void setExpression(Expression expression) {
        this.expression = expression;
    }

    @Override
    public List<ASTNode> getChildren() {
        List<ASTNode> result = new LinkedList<>();
        result.add(expression);
        return result;

    }
}
