package whyML.ast.expression;

import whyML.ast.ASTNode;
import whyML.ast.spec.Spec;
import whyML.ast.types.Type;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Michael on 12/3/2015.
 */
public class AnyExpression extends Expression {

    Type type;
    //can be empty, shouldn't be null
    List<Spec> specs;

    /**
     *
     * @param type of this expression
     * @param specs can be empty, shouldnt be null.
     */
    public AnyExpression(Type type, List<Spec>specs){
        this.type = type;
        if(specs != null){
            this.specs = specs;
        }else{
            this.specs = new LinkedList<>();
        }

    }

    /**
     * specs is set to empty list.
     * @param type of this expression
     */
    public AnyExpression(Type type){
        this.type = type;
        this.specs = new LinkedList<>();
    }

    public List<Spec> getSpecs() {
        return specs;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public List<ASTNode> getChildren() {
        List<ASTNode> children = new LinkedList<>();
        children.addAll(specs);
        children.add(type);
        return children;
    }
}
