package whyML.ast.expression;


import whyML.ast.ASTNode;
import whyML.ast.spec.Invariant;
import whyML.ast.spec.Variant;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;

public class WhileLoopExpression extends Expression{

    private Expression whileCondition;
    //can be empty
    private List<Invariant> invariants;
    //can be null
    private Variant variant;
    private Expression loopExpression;


    @Override
    public List<ASTNode> getChildren() {
        List<ASTNode> children = new LinkedList<>();
        children.add(whileCondition);
        children.addAll(invariants);
        if(variant != null){
            children.add(variant);
        }
        children.add(loopExpression);
        return children;
    }

    public WhileLoopExpression(Expression whileCondition, List<Invariant> invariants, Variant variant, Expression loopExpression){
        this.whileCondition = whileCondition;
        this.invariants = invariants;
        this.variant = variant;
        this.loopExpression = loopExpression;
    }

    public WhileLoopExpression(Expression whileCondition, List<Invariant> invariants, Expression loopExpression){
        this.whileCondition = whileCondition;
        this.invariants = invariants;
        this.variant = null;
        this.loopExpression = loopExpression;
    }

    public WhileLoopExpression(Expression whileCondition, Expression loopExpression){
        this.whileCondition = whileCondition;
        this.invariants = new LinkedList<>();
        this.variant = null;
        this.loopExpression = loopExpression;
    }

    public Expression getLoopExpression() {
        return loopExpression;
    }

    public Expression getWhileCondition() {
        return whileCondition;
    }

    /**
     * can be empty.
     * @return
     */
    public List<Invariant> getInvariants() {
        return invariants;
    }

    /**
     * can be null
     * @return
     */
    public Variant getVariant() {
        return variant;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }
}
