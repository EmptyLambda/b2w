package whyML.ast.spec;


import whyML.ast.ASTNode;
import whyML.ast.terms.Term;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;

public class WritesSpec extends Spec {

    private List<Term> terms;

    public WritesSpec(Term term){
        this.terms = new LinkedList<>();
        terms.add(term);
    }

    public WritesSpec(List<Term> terms){
        this.terms = terms;
    }

    public List<Term> getTerms() {
        return terms;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public List<ASTNode> getChildren() {
        List<ASTNode> children = new LinkedList<>();
        children.addAll(terms);
        return children;
    }
}
