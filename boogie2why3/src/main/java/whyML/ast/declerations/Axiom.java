package whyML.ast.declerations;


import whyML.ast.ASTNode;
import whyML.ast.formula.Formula;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;

public class Axiom extends Declaration {

    private String ident;
    private Formula formula;

    public Axiom(String ident, Formula formula){
        this.ident = ident;
        this.formula = formula;
    }

    public String getIdent() {
        return ident;
    }

    public void setIdent(String ident) {
        this.ident = ident;
    }

    public Formula getFormula() {
        return formula;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public List<ASTNode> getChildren() {
        List<ASTNode> children = new LinkedList<>();
        children.add(formula);
        return children;
    }
}
