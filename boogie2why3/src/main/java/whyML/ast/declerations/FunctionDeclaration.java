package whyML.ast.declerations;


import whyML.ast.ASTNode;
import whyML.ast.Label;
import whyML.ast.Param;
import whyML.ast.formula.Formula;
import whyML.ast.terms.Term;
import whyML.ast.types.Type;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;

public class FunctionDeclaration extends Declaration{

    //can be null if there is no formula
    private Term optionalTerm;
    private String name;
    private Label label;
    private List<Param> params;
    private Boolean isInfixOrPrefix;
    private Type returnType;


    @Override
    public List<ASTNode> getChildren() {
        List<ASTNode> children = new LinkedList<>();
        if(optionalTerm != null){
            children.add(optionalTerm);
        }
        children.add(label);
        children.addAll(params);
        children.add(returnType);
        return children;
    }

    public FunctionDeclaration(String name, Label label, List<Param> params, Term term, Type returnType, Boolean isInfixOrPrefix){
        this.name = name;
        this.label = label;
        this.params = params;
        this.optionalTerm = term;
        this.isInfixOrPrefix = isInfixOrPrefix;
        this.returnType = returnType;
    }


    /**
     * Create a predice without label and without a formula. (i.e. there is no equal sign after the predicate definition)
     * @param name
     * @param params
     */
    public FunctionDeclaration(String name, List<Param> params, Type returnType, Boolean isInfixOrPrefix){
        //TODO add "with" clause, which we don't need for our translation.
        this.name = name;
        this.params = params;
        this.optionalTerm = null;
        this.label = new Label(false);
        this.isInfixOrPrefix = isInfixOrPrefix;
        this.returnType = returnType;
    }

    /**
     * Create a predicate without a label.
     * @param name
     * @param params
     * @param term
     */
    public FunctionDeclaration(String name, List<Param> params, Term term, Type returnType, Boolean isInfixOrPrefix){
        this.name = name;
        this.label = new Label(false);
        this.params = params;
        this.optionalTerm = term;
        this.returnType = returnType;
        this.isInfixOrPrefix = isInfixOrPrefix;
    }


    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    public Label getLabel() {
        return label;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Term getOptionalTerm() {
        return optionalTerm;
    }

    public List<Param> getParams() {
        return params;
    }

    public Boolean getIsInfixOrPrefix() {
        return isInfixOrPrefix;
    }

    public Type getReturnType() {
        return returnType;
    }
}
