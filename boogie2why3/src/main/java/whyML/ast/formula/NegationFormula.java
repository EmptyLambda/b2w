package whyML.ast.formula;


import whyML.ast.ASTNode;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;

public class NegationFormula extends Formula{

    //the formula to be negated
    private Formula formula;

    /**
     *
     * @param formula is the formula to be negated.
     */
    public NegationFormula(Formula formula){
        this.formula = formula;
    }

    public Formula getFormula() {
        return formula;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public List<ASTNode> getChildren() {
        List<ASTNode> children = new LinkedList<>();
        children.add(formula);
        return children;
    }
}
