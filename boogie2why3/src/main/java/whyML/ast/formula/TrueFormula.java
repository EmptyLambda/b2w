package whyML.ast.formula;


import whyML.ast.ASTNode;
import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;

public class TrueFormula extends Formula {

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public boolean equals(Object obj) {
        //return super.equals(obj);
        return (obj instanceof TrueFormula);
    }

    @Override
    public List<ASTNode> getChildren() {
        return new LinkedList<>();
    }
}
