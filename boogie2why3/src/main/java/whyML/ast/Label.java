package whyML.ast;


import whyML.astvisitor.ASTVisitor;

import java.util.LinkedList;
import java.util.List;

public class Label implements  ASTNode {

    private Boolean hasLabel;

    public Label(Boolean hasLabel){
        this.hasLabel = hasLabel;
    }

    public Boolean getHasLabel() {
        return hasLabel;
    }

    //TODO toString


    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public List<ASTNode> getChildren() {
        return new LinkedList<>();
    }
}
