var x: bool;
procedure p (x: int) returns (y:int)
ensures x == y; //shadowed x of type int
{
  y := x; //shadowed x of type int
}
