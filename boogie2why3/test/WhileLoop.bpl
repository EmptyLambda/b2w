var v: int where v > 0;


procedure p(x: int) returns (res:int)
requires x > 100;
free requires x > 5;
{
    while(true)
    invariant x > 100;
    {
        assert x > 50;
    }
}
