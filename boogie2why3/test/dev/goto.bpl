procedure F(n: int) returns (r: int)
ensures n == r;
{
  goto a,b,c;
  a:
  assume n == r;
  return;
  b:
  assume r == 0;
  assume n == 0;
  return;
  c:
  assume n == r;
}
