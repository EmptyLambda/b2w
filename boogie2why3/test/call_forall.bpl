procedure lemma(x:int, y:int)
requires y > x;
ensures y >= x;
{
}

procedure p() returns (res:bool)
{
    call forall lemma(*,1);
}

procedure p_2() returns (res:bool)
{
    var m: [bool]int;
    call forall lemma(*,m[true]);
}

procedure explicit_p() returns (res:bool)
{
    assume (forall x:int :: (1 > x) ==> (1>=x));
}

procedure lemma2(m:[int]bool, i:int)
ensures m[i] == true;
{

}

procedure p2_1()
{
    var m2: [int]bool;
    call forall lemma(m2,5);
}

procedure p2_2()
{

    call forall lemma(*,*);
}