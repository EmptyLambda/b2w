procedure p1 (x:int) returns (res: int);

procedure p2 () returns (result: int)
ensures result == 10;
{
    var i: int;
    i := 0;
    result := 0;
    while (i < 10)
        invariant i <= 10;
        invariant 0 <= i;
        //next invariant is not verifiable, so we might want to state it as free.
        free invariant i == result;
    {
        call result := p1 (result);
        i := i + 1;
    }
}