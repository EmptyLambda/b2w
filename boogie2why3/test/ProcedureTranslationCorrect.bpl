procedure p1(x:bool) returns (y:int)
//ensures y == 0;
{
  y := 0;
}

procedure p2(x:bool) returns (y:int)
ensures y == 0;
{
  call y := p1(true);
}