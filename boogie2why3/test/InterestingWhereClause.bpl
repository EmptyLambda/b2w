procedure p(x:int) returns (y:int where y > 5)
ensures y<5;
{
  assert y > 5;
  y := 3;
}


procedure p2(x:int) returns (y:int)
{
  var pRes: int;
  call pRes := p(x);
  assert false;//verifiable because of WhereClause and Ensures clause of p which contradict.
}

procedure p3(x:int) returns (y:int where y > 5)
requires x > 0 && x < 5;
ensures y<5;
{
  var i: int;
    if(x == 1){
      y := 4;
      return;
    }else{
      call i := p3(x-1);
      assert false;
      y := i;
      return;
    }
}