//example 1:
const N: int;
axiom 0 <= N;

// Erroneously verifies with Boogie 2.3.0.61016 and Z3 4.4.2
procedure vacuous_post()
 ensures (forall k, l: int :: 0 <= k && k <= l && l < N ==> N < N);
{
 var x: int;
 x := -N;
 while (x != x) {
 }
}

//example 2:
function inv(x : real) returns (real)
{ 1/x }

procedure lemma(x, y : real)
requires x != 0.0;
ensures inv(x)**y == inv(x**y);
{ }


//example 3:
procedure trivial_inv()
{
    var i : int;
    i := 0;
    while (i < 10)
    invariant 0 <= i && i <= 10;
    invariant (exists j : int :: i == 2*j);
        { i := i + 2; }
}