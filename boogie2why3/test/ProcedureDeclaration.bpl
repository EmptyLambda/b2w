procedure mp_p(x: int) returns (res:int)
requires x > 0;
ensures res < 3;
{
  assume  res < 3;
}

procedure mp_p2(x1,x2: int, y1: bool, y2: bool) returns (res1,res2:int, res3:bool, res4:int)
requires x1 > 0;
free requires x1 > 5;
ensures res1 < 3;
requires x2 < 0;
ensures res3;
{
  assume  res1 < 3;
  assume res3;
  assert x1 > 0;
}

procedure mp_p3(x,y: int)
requires x > 0;
ensures y < 3;
{
  assume  y < 3;
}


procedure mp_p4<a>(x,y: a)
ensures y <: x;
{
  assume  y <: x;
}

procedure mp_p5<a,b>(x1,x2:a, y1:b) returns (z1,z2:a, z3:int, z4,z5:b)
ensures z1 <: z2;
{
  assume  z1 <: z2;
}


//whereClauses
//whereClause for input arguments behave the same as free require clauses.
procedure mp_p6(x:int where x > 3) returns (y:int where y == 6)
ensures y > 5;
{
  assert x > 3;
  y := 6;
  assert x > 3;
}


procedure mp_p7(x:int) returns (y:int)
ensures y == 6;
{
  call y := mp_p6(0);
}

procedure mp_p8(x: int) returns (res:int)
requires x > 0;
ensures res < 3;
{
  var b:bool;
  if(*){
    res := -1;
  }else if(x > 3){
    res := -2;
  }else if(*){
    res := -3;
  }else{
    res := 2;
  }
}
