//TODO needs more examples. (with RealLiteral etc)


function fib(n:int): int{
      if n==0 then
        0
      else
        if n==1 then
          1
         else
          fib(n-2) + fib(n-1)
}

function f(i:int):real;

axiom 5 + 3 == 8;
axiom 5 -3 ==2;
axiom 2*2==4;

axiom f(0) + f(1) > f(1);
axiom f(0) - f(1) == f(2);
axiom f(3) * f(4) >= f(7);

axiom 1.2 * 1.2 <= 2.1* 2.1;